var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var path = require('path');

module.exports = {
	entry: {
		'build/popup': './popup.js',
		'build/background': './background.js',
		'build/options': './options.js'
	},
	output: {
		path: './',
		filename: '[name].js'
	},
	context: path.join(__dirname, 'src'),
	plugins: [
		new Clean(['build']),
		new Copy([{
			from: 'manifest.json',
			to: 'build/manifest.json'
		},
		{
			from: '*.html',
			to: 'build'
		},
		{
			from: 'images/*',
			to: 'build'
		}])
	]
};
