var AudioProcessor = require('./AudioProcessor');
var AudioVisual = require('./AudioVisual');
/**
 * AudioRecorder records a WebAudio stream at a certain audio sampleRate to a buffer
 * AudioRecorder can be stopped, and restarted. Resetting the AudioRecorder trashes
 * the current buffer
 */
function AudioRecorder(options) {
	this.audioContext = new AudioContext();
	this.stream = options.stream;
	this.logoEl = options.logoEl;
	this.listening = false;
	this.audioProcessor = (new AudioProcessor({
		audioContext: this.audioContext,
		outputRate: options.outputRate || 16000
	}));
	this.visual = new AudioVisual({
		audioContext: this.audioContext,
		logoEl: this.logoEl
	});
}

AudioRecorder.prototype.start = function () {
	if (this.listening !== true) {
		console.log('starting recording...');
		this.source = this.audioContext.createMediaStreamSource(this.stream);
		this.source.connect(this.visual.getNode());
		this.visual.getNode().connect(this.audioProcessor.getProcessor());
		this.audioProcessor.getProcessor().connect(this.audioContext.destination);
		this.visual.start();
		this.listening = true;
	}
};

AudioRecorder.prototype.stop = function () {
	if (this.listening === true) {
		console.log('stopping recording');
		this.source.disconnect(this.visual.getNode());
		this.visual.getNode().disconnect(this.audioProcessor.getProcessor());
		this.audioProcessor.getProcessor().disconnect(this.audioContext.destination);
		this.stream.getAudioTracks()[0].stop(); // Todo: how to reverse this?
		// https://developer.mozilla.org/en-US/docs/Web/API/AudioContext/close
		this.audioContext.close();
		this.visual.stop();
		this.listening = false;
	}
};

AudioRecorder.prototype.reset = function () {
	if (this.listening === true) {
		this.stop();
	}
	this.audioProcessor.reset();
};

AudioRecorder.prototype.getRecording = function () {
	return this.audioProcessor.getBuffer();
};

AudioRecorder.prototype.isListening = function () {
	return this.listening;
};

module.exports = AudioRecorder;
