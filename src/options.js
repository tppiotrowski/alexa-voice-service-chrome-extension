var OAuthClient = require('./oAuthClient');
var oAuthClient = new OAuthClient();

var mvc = {
	attributes: {
		microphonePermission: false,
		oauthPermission: false
	},
	set: function (key, value) {
		this.attributes[key] = value;
		this.render();
	},
	render: function () {
		var requirementsMet = 0;
		var microphoneContainer = document.getElementById('microphoneContainer');
		var oauthContainer = document.getElementById('oauthContainer');
		if (this.attributes.microphonePermission === true) {
			requirementsMet++;
			microphoneContainer.className = 'enabled';
		} else {
			microphoneContainer.className = 'disabled';
		}
		var oauthButtonText = '';
		if (this.attributes.oauthPermission === true) {
			requirementsMet++;
			oauthContainer.className = 'enabled';
			oauthButtonText = 'Revoke access to Amazon.com account';
		} else {
			oauthContainer.className = '';
			oauthButtonText = '';
		}
		document.getElementById('authenticateWithAmazon').textContent = oauthButtonText;
		document.getElementById('requirements').textContent = requirementsMet;
	}
};

function updateOAuthPermission(promise) {
	promise.then(function () {
		mvc.set('oauthPermission', true);
	})
	.catch(function () {
		mvc.set('oauthPermission', false);
	});
}

window.onload = function () {
	navigator.webkitGetUserMedia({ audio: true }, function (stream) {
		var tracks = stream.getAudioTracks();
		for (var i = 0; i < tracks; i++) {
			tracks[i].stop();
		}
		mvc.set('microphonePermission', true);
	}, function (err) {
		if (err === SecurityError) {
			// permission was denied by user or system
		} else if (err === NotFoundError) {
			// no devices available for recording audio
		}
		mvc.set('microphonePermission', false);
	});

	var promise = oAuthClient.getToken();
	updateOAuthPermission(promise);

	var authenticateButton = document.getElementById('authenticateWithAmazon');
	authenticateButton.addEventListener('click', function () {
		if (mvc.attributes.oauthPermission === false) {
			var promise = oAuthClient.authenticate();
			updateOAuthPermission(promise);
		} else {
			oAuthClient.revokeAccess().then(function () {
				mvc.set('oauthPermission', false);
			});
		}
	});
};
