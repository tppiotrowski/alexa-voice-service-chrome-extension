function AudioVisual(options) {
	this.audioContext = options.audioContext || new AudioContext();
	this.logoEl = options.logoEl;
	this.node = this.audioContext.createAnalyser();
	this.visualBufLength = this.node.frequencyBinCount;
	this.visualArray = new Uint8Array(this.visualBufLength);
}

AudioVisual.prototype.getNode = function () {
	return this.node;
};

AudioVisual.prototype.start = function () {
	this.listening = true;
	this.logoEl.style.animationDuration = '0s';
	this.drawVisual = this.draw();
};

AudioVisual.prototype.stop = function () {
	this.logoEl.style.animationDuration = '1s';
	this.listening = false;
};

AudioVisual.prototype.draw = function () {
	this.node.getByteFrequencyData(this.visualArray);
	var max = Math.max.apply(null, this.visualArray);
	max = max / 256.0 * 0.9 + 1;
	if (this.listening === true) {
		window.requestAnimationFrame(this.draw.bind(this));
	} else {
		max = 1;
	}
	this.logoEl.style.transform = 'scale(' + max + ',' + max + ')';
};

module.exports = AudioVisual;
