/**
 * AudioFile class converts WebAudio data into a format required by AVS api
 * This format is Audio/L16 https://tools.ietf.org/html/rfc2586
 * My understanding is that this format is the same as WAVE format, which itself
 * is based on a universal RIFF format for storing data as chunks
 *
 * Much of this code is modified from:
 * http://typedarray.org/from-microphone-to-wav-with-getusermedia-and-web-audio/
 */

function AudioFile(options) {
	this.data = options.data;
	this.outputRate = options.outputRate;
}

AudioFile.prototype.toArray = function () {
	var headerSizeInBytes = 44;
	var header = new ArrayBuffer(headerSizeInBytes);
	var view = new DataView(header);
	// http://soundfile.sapp.org/doc/WaveFormat/
	// chunkid
	writeUTFBytes(view, 0, 'RIFF'); // chunkid
	view.setUint32(4, 44 + this.data.length, true); // chunk size (header + data)
	writeUTFBytes(view, 8, 'WAVE'); // format
	// subchunk1
	writeUTFBytes(view, 12, 'fmt '); // subchunk1id
	view.setUint32(16, 16, true); // subchunk1 size
	view.setUint16(20, 1, true); // audio format
	view.setUint16(22, 1, true); // numchannels
	view.setUint32(24, this.outputRate, true); // samplerate
	view.setUint32(28, this.outputRate * 2, true); // byte rate
	view.setUint16(32, 2, true); // block align
	view.setUint16(34, 16, true); // bits per sample
	// subchunk2
	writeUTFBytes(view, 36, 'data'); // subchunk2id
	view.setUint32(40, this.data.length * 2, true); // subchunk2 size

	var file = new Int16Array((headerSizeInBytes / 2) + this.data.length);
	file.set(new Int16Array(header));
	file.set(this.data, (headerSizeInBytes / 2));
	return file;
};

AudioFile.prototype.toBlob = function () {
	return new Blob([this.toArray()], { type: 'audio/l16' });
};

function writeUTFBytes(view, offset, string) {
	var lng = string.length;
	for (var i = 0; i < lng; i++) {
		view.setUint8(offset + i, string.charCodeAt(i));
	}
}

module.exports = AudioFile;
