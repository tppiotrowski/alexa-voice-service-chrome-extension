var OAuthClient = require('./oAuthClient');
var AudioRecorder = require('./AudioRecorder');
var AudioFile = require('./AudioFile');
var AudioVisual = require('./AudioVisual');
var Settings = require('./settings');
var chrome = require('./chrome');

var oAuthClient = new OAuthClient();

var logoEl = document.getElementById('logo');
var statusEl = document.getElementById('status');
var recordEl = document.getElementById('record');
var stopEl = document.getElementById('stop');
var settingsEl = document.getElementById('settings');
var responseEl = document.getElementById('response');

main();

function str2ab(str) {
	var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
	var bufView = new Uint16Array(buf);
	for (var i = 0, strLen = str.length; i < strLen; i++) {
		bufView[i] = str.charCodeAt(i);
	}
	return buf;
}

function main() {
	var recorder; // will record the audio
	var oAuthToken;

	settingsEl.addEventListener('click', function () {
		if (typeof recorder !== undefined && recorder.isListening() === true) {
			recorder.stop();
		}
		chrome.runtime.openOptionsPage();
	});

	try {
		stopEl.addEventListener('click', function () {
			logoEl.style.animationName = 'spin';
			logoEl.style.animationPlayState = 'running';
			statusEl.textContent = 'Processing...';
			stopEl.disabled = true;
			recordEl.disabled = false;
			recorder.stop();
			var buffer = recorder.getRecording();
			var fileBlob = new AudioFile({ data: buffer, outputRate: Settings.audioRate }).toBlob();
			send(fileBlob, oAuthToken);
		});

		recordEl.addEventListener('click', function () {
			recordEl.disabled = true;
			stopEl.disabled = false;
			if (typeof recorder === 'undefined' || recorder.isListening() === false) {
				statusEl.textContent = 'Attempting to use microphone...';
				navigator.webkitGetUserMedia({ audio: true }, function (stream) {
					statusEl.textContent = 'Listening...';
					recorder = new AudioRecorder({ stream: stream, outputRate: Settings.audioRate, logoEl: logoEl });
					recorder.start();
					oAuthClient.getToken().then(function (token) {
						oAuthToken = token;
					}).catch(function (err) {
						recordEl.disabled = true;
						stopEl.disabled = true;
						recorder.stop();
						chrome.runtime.openOptionsPage();
						throw err;
					});
				}, function (err) {
					throw err;
				});
			}
		});

		recordEl.dispatchEvent(new Event('click'));
	} catch (reason) {
		chrome.runtime.openOptionsPage();
		// if (reason instanceof PermissionDeniedError) {
		// 	chrome.runtime.openOptionsPage();
		// }
	};
};

function send(fileBlob, oAuthToken) {
	var message = {
		messageHeader: {
			deviceContext: [{
				name: 'playbackState',
				namespace: 'AudioPlayer',
				payload: {
					streamId: '',
					offsetInMilliseconds: '0',
					playerActivity: 'IDLE'
				}
			}]
		},
		messageBody: {
			profile: 'alexa-close-talk',
			locale: 'en-us',
			format: 'audio/L16; rate=16000; channels=1'
		}
	};
	var messageBlob = new Blob([JSON.stringify(message)], {
		type: 'application/json; charset=UTF-8'
	});

	var fd = new FormData();
	fd.append('request', messageBlob);
	fd.append('audio', fileBlob);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'https://access-alexa-na.amazon.com/v1/avs/speechrecognizer/recognize', true);
	xhr.setRequestHeader('Authorization', 'Bearer ' + oAuthToken);
	xhr.responseType = 'arraybuffer';
	xhr.onload = function (blob) {
		if (xhr.status === 200) {
			statusEl.textContent = 'Processing reply...';
			console.log(xhr.getAllResponseHeaders());
			var match = xhr.getResponseHeader('Content-Type').match(/boundary=([^;]+)/);
			var boundary = match[1];
			var responseText = String.fromCharCode.apply(null, new Uint8Array(xhr.response));
			console.log(responseText.length);
			var parts = responseText.split('--' + boundary);
			var json = JSON.parse(parts[1].substring(parts[1].search(/json/) + 8));
			console.log('JSON:', json);
			var idx = parts[2].search(/mpeg/g);
			var mp3 = (new Uint8Array(xhr.response)).slice(idx + 8);
			var alexaBlob = new Blob([mp3], { type: 'audio/mpeg' });
			var audio = new Audio();
			var context = new AudioContext();
			var source = context.createMediaElementSource(audio);
			var visual = new AudioVisual({
				audioContext: context,
				logoEl: logoEl
			});
			source.connect(visual.getNode());
			visual.getNode().connect(context.destination);
			audio.src = window.URL.createObjectURL(alexaBlob);
			statusEl.textContent = 'Playing...';
			audio.play();
			visual.start();
			audio.addEventListener('ended', function () {
				source.disconnect(visual.getNode());
				context.close();
				visual.stop();
				logoEl.style.animationDuration = '0s';
				logoEl.style.animationName = '';
				statusEl.textContent = 'Press speak.';
			});
		} else {
			logoEl.style.animationDuration = '0s';
			logoEl.style.animationName = '';
			statusEl.textContent = 'Request was not understood by Alexa';
		}
	};
	statusEl.textContent = 'Sending...';
	xhr.send(fd);
}
