var chrome = require('./chrome');
var DEV_MODE = false;

var checkIfDev = new Promise(function (resolve, reject) {
	chrome.management.getSelf(function (result) {
		DEV_MODE = (result.installType === 'development');
		resolve(DEV_MODE);
	});
});

chrome.runtime.onInstalled.addListener(function (details) {
	if (details.reason === 'install') {
		chrome.runtime.openOptionsPage();
	}
});
