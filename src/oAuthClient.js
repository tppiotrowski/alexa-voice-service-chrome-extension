var Settings = require('./settings');
var chrome = require('./chrome');

function OAuthClient() {
}

OAuthClient.prototype = {
	/**
	 * Returns unique number identifying extension installation
	 * A serial number is required for AVS when granting access tokens
	 * @returns {Number}
	 */
	getUniqueId: function () {
		if (localStorage.getItem('serial') === null) {
			localStorage.setItem('serial', Math.random().toString(36).slice(2));
		};
		return localStorage.getItem('serial');
	},

	getLoginUrl: function (options) {
		var loginUrl = 'https://www.amazon.com/ap/oa';
		var scope = 'alexa:all';
		// In order to provide auto refresh for OAuth tokens, we need to provide
		// a webservice to hold the clientSecret. For now we must use hourly token
		var responseType = 'code';
		var scopeData = JSON.stringify({
			'alexa:all': {
				productID: Settings.productId,
				productInstanceAttributes: {
					deviceSerialNumber: this.getUniqueId()
				}
			}
		});
		var redirectUrl = chrome.identity.getRedirectURL();

		return loginUrl +
			'?client_id=' + Settings.clientId +
			'&scope=' + encodeURIComponent(scope) +
			'&scope_data=' + encodeURIComponent(scopeData) +
			'&response_type=' + encodeURIComponent(responseType) +
			'&redirect_uri=' + encodeURIComponent(redirectUrl);
	},

	authenticate: function () {
		var self = this;
		return new Promise(function (resolve, reject) {
			var options = {
				url: self.getLoginUrl(),
				interactive: true
			};
			chrome.identity.launchWebAuthFlow(options, function (url) {
				var expiresIn;
				var accessCodeMatch = url.match(/code=([^&]*)/);
				var accessCode;
				if (accessCodeMatch !== null && accessCodeMatch.length == 2) {
					accessCode = decodeURIComponent(accessCodeMatch[1]);
				}
				// We have obtained an access code, now we use it to obtains access/refresh tokens
				var options = {
					method: 'POST',
					body: accessCode
				};

				fetch(Settings.tokenUrl, options)
					.then(function (response) {
						return response.json();
					})
					.then(function (json) {
						self.setToken(json['access_token'], json['refresh_token'], json['expires_in']);
						resolve(json['access_token']);
					});
			});
		});
	},

	revokeAccess: function () {
		return new Promise(function (resolve, reject) {
			chrome.storage.sync.remove([
				'oauthToken',
				'oauthRefreshToken',
				'oauthTokenExpiration'
			], function () {
				resolve();
			});
		});
	},

	setToken: function (token, refreshToken, duration) {
		var durationInMs = duration * 1000;
		var expiration = Date.now() + durationInMs;
		chrome.storage.sync.set({
			oauthToken: token,
			oauthRefreshToken: refreshToken,
			oauthTokenExpiration: expiration
		});
	},

	getToken: function () {
		var self = this;
		return new Promise(function (resolve, reject) {
			chrome.storage.sync.get([
				'oauthToken',
				'oauthRefreshToken',
				'oauthTokenExpiration'
			], function (data) {
				if (data && data.oauthToken && data.oauthRefreshToken) {
					if (data.oauthTokenExpiration < Date.now()) {
						var options = {
							method: 'POST',
							body: data.oauthRefreshToken
						};
						fetch(Settings.refreshTokenUrl, options)
							.then(function (response) {
								return response.json();
							})
							.then(function (json) {
								self.setToken(json['access_token'], json['refresh_token'], json['expires_in']);
								resolve(json['access_token']);
							})
							.catch(function (err) {
								reject(err);
							});
					} else {
						resolve(data.oauthToken);
					}
				} else {
					reject('No token');
				}
			});
		});
	}
};

module.exports = OAuthClient;
