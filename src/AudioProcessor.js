function AudioProcessor(options) {
	this.audioContext = options.audioContext;
	this.buffer = new Int16Array();
	this.sampleRate = options.audioContext.sampleRate + 0.0;
	this.outputRate = options.outputRate;
	this.recorder = options.audioContext.createScriptProcessor(2048, 1, 1);
	this.leftover = new Float32Array();
	this.recorder.onaudioprocess = function (e) {
		var newData = this.reSampleAverage(e.inputBuffer.getChannelData(0));

		var bufLength = this.buffer.length;
		var combined = new Int16Array(bufLength + newData.length);
		combined.set(this.buffer);
		combined.set(newData, bufLength);
		this.buffer = combined;
	}.bind(this);
}

AudioProcessor.prototype.getProcessor = function () {
	return this.recorder;
};

/**
 * Naive resampling. Just pick every nth sample. No averaging.
 */
AudioProcessor.prototype.reSample = function (data) {
	var s = 0;
	var o = this.sampleRate / this.outputRate;
	var u = Math.ceil(data.length * this.outputRate / this.sampleRate);
	var a = new Int16Array(u);
	for (var i = 0; i < u; i++) {
		a[i] = floatTo16BitPCM(data[Math.floor(s)]);
		s += o;
	}
	return a;
};

/**
 * If using every 5th sample, use an average of the 5 values.
 */
AudioProcessor.prototype.reSampleAverage = function (data) {
	var combined = new Float32Array(this.leftover.length + data.length);
	combined.set(this.leftover);
	combined.set(data, this.leftover.length);
	var s = 0;
	var o = this.sampleRate / this.outputRate;
	var u = Math.floor(data.length * this.outputRate / this.sampleRate);
	var a = new Int16Array(u);
	var curIndex;
	var nextIndex;
	for (var i = 0; i < u; i++) {
		curIndex = Math.floor(s);
		nextIndex = Math.floor(s + o);
		a[i] = floatTo16BitPCM(data.slice(curIndex, nextIndex).reduce((pv, cv) => pv + cv, 0) / (nextIndex - curIndex));
		s += o;
	}
	this.leftover = data.slice(Math.floor(s));
	return a;
};

AudioProcessor.prototype.getBuffer = function () {
	return this.buffer;
};

AudioProcessor.prototype.reset = function () {
	this.buffer = [];
};

function floatTo16BitPCM(input) {
	var s = Math.max(-1, Math.min(1, input));
	return s < 0 ? s * 0x8000 : s * 0x7FFF;
}

module.exports = AudioProcessor;
