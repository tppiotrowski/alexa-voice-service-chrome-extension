#Alexa Voice Service Chrome extension

Integrate Alexa Voice Service into Google Chrome

##Setup

Provide a settings.js file inside of /src folder specifying the OAuth clientId
and productId you set up for your app. This extension uses Authorization Code
Grant described [here](https://developer.amazon.com/public/solutions/alexa/alexa-voice-service/docs/authorizing-your-alexa-enabled-product-from-a-website)
which requires an itermediary to refresh the OAuth token via Amazon periodically.
You must provide your own end points for granting and refreshing tokens.

```
var Settings = function () {
	return {
		clientId: 'amzn1.application-oa2-client....bd',
		productId: '',
		tokenUrl: 'https://www.yourwebservice.com/grant.jsp',
		refreshTokenUrl: 'https://www.yourwebservice.com/refresh.jsp',
		audioRate: 16000
	};
}();
```